import React, { Component } from 'react';
import './register.component.scss';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import { Row, Col } from 'antd';
const FormItem = Form.Item;

export class RegisterComponent extends Component{

    constructor() {
        super();
    }
    
    render() {
    
        return (<div className="register-page">
            <Row className="page-header shadow-light">
                <Col span={24}>
                    <div className="logo">
                        <img src="/images/logo.png"/>
                    </div>
                    <div className="title">
                        <h3>用户注册</h3>
                    </div>
                </Col> 
            </Row>
        </div>);
        
    }
}