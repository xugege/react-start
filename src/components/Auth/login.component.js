
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'antd';
import './login.component.scss';
import { LoginService} from '../../http/service-proxies';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
const FormItem = Form.Item;
import { Helper } from '../../common/helper';
import { message,Spin,Alert } from 'antd';


class LoginComponent extends Component{

    constructor() {
        super();
        this._loginService = new LoginService();
        this.state = {
          logining: false,
          captcha:'/captcha'
        };
    }

    register() {
        let that = this;
        that.props.router.push('/register');
    }

    login() {
        let that = this;
      let form = that.props.form;

        that.showLoading();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                // console.log('Received values of form: ', values);
                that._loginService.login(values.studentNo, values.password)
                    .then((result) => {
                    that.hideLoading();
                    if (!result) {
                        // console.log('登录失败')
                        message.warning('登录失败',1)
                    } else {
                        //登录成功
                        that.props.router.push('/counter');
                    }

                })
            } else {
                that.hideLoading();
            }
          });
    }

    showLoading() {
        this.setState({
            logining: true
        });
    }

    hideLoading() {
        this.setState({
            logining: false
        });
    }

    componentWillMount() {
        let props = this.props;
        let form = this.props.form;

    }

    getCaptcha() {
      this.setState(Object.assign({}, this.state, {
        captcha:'/captcha?t='+Math.random()
      }));
    }

    render() {

        let props = this.props;
        let query = props.location.query;
        let returnUrl = !query || !query.returnUrl ? '' : query.returnUrl;

        const { getFieldDecorator } = this.props.form;

        return (


            <div className="login-page">
                <div className="login-logo">
                    <img src="/images/logo.png" />
                    <h3>下一代WEB智能</h3>
                </div>    
                <Spin tip="登陆中..." spinning={this.state.logining} wrapperClassName="full-height">
                    <div className="login-box shadow-dark">
                        <div className="login-box__over"></div>
                        <Form className="login-form">
                            <FormItem>
                            {getFieldDecorator('userName', {
                                    rules: [{ required: true, message: '请输入用户名' }],
                                })(
                                    <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="用户名" />
                                )}
                            </FormItem>
                            <FormItem>
                                {getFieldDecorator('password', {
                                    rules: [{ required: true, message: '请输入密码!' }],
                                })(
                                    <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="密码" />
                                )}

                            </FormItem>
                            <FormItem>
                                {getFieldDecorator('code', {
                                    rules: [{ required: true, message: '请输入验证码!' }],
                                })(
                                  <Input prefix={<Icon type="heart" style={{ color: 'rgba(0,0,0,.25)' }} />} type="text" placeholder="验证码" />
                                )}

                            </FormItem>
                            <FormItem>
                                <img src={this.state.captcha} onClick={this.getCaptcha.bind(this)}/>
                                {/* <Checkbox>记住我</Checkbox> */}
                                <a className="login-form-forgot" href="javascript:;">忘记密码?</a>
                            </FormItem>
                            <FormItem>
                                <Button onClick={this.login.bind(this)} type="primary" htmlType="submit" className="login-form-button">
                                    登录
                                </Button>
                                <a onClick={this.register.bind(this)} href="javascript:;" className="login-form-register">注册</a>
                            </FormItem>
                        </Form>
                    </div>
                </Spin>

            </div>
        );
    }

}

export default Form.create()(LoginComponent);
