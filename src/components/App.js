import React, { Component } from 'react';
import PropTypes from 'prop-types';

export class App extends Component{


    componentWillMount() {
        //this.props.router.push('/app');
    }

    render() {
        return (<div>
            {children}
        </div>);
    }
}


App.propTypes = {
    children: PropTypes.element.isRequired
  }
  
  export default App;