import React, { Component }  from 'react'
import { IndexLink, Link } from 'react-router'
import './Header.scss'
import { Layout ,Menu, Icon } from 'antd';

const { Header} = Layout;


export class PageHeader extends Component {

  getInitialState () {
    return {
      opacity: 1.0
    };
  }

  componentDidMount(){
    console.log(this)
  }


  render(){
    return (<Header>
      <Menu mode="horizontal">
          <Menu.Item key="home">首页</Menu.Item>
      </Menu>
    </Header>);
  }
}
 
export default PageHeader
