import {
  HttpProxy
} from './http-proxy';
import {
  AppConsts
} from '../store/appConsts';
import { Helper } from '../common/helper'

export class LoginService {


  constructor() {
    this.baseUrl = AppConsts.remoteApiUrl;
  }

  login(userName, password) {

    return new Promise((resolve, reject) => {
      HttpProxy.post(this.baseUrl + 'auth/login', {
        userName,
        password
      }, {}, false).then((data) => {
        if (data && data.result && data.result.success) {
          Helper.setToken(data.result.token);
          resolve(true);
        }
        else resolve(false);
      }).catch(err => {
        console.log(err);
        resolve(false);
      });
    });

  }



}
