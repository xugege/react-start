export class AppConsts {

  static baseUrl = '';
  static remoteApiUrl = '';
  static menu = {
    mode: 'horizontal',
    theme: 'light',
    default: 'home',
    items: [{
      key: 'home',
      path: '/'
    }, {
      key: 'counter',
      path: '/counter'
    }]
  };

  static login_path = ['/counter'];
  static cookie_cultureName = "Abp.Localization.CultureName";
  static cookie_tenant_id = "Abp.TenantId";
  static cookie_auth_token = "Registration.Auth.Token";
  static store_login_key = "store.login.key";
}

export default {
  AppConsts
};
