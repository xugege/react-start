import React from 'react'
import DuckImage from '../assets/Duck.jpg'
import './HomeView.scss'
import { DatePicker,Button } from 'antd';

function onChange(date, dateString) {
  console.log(date, dateString);
}

export const HomeView = () => (
  <div>
    <h4>This is a duck, because Redux!</h4>
    <DatePicker onChange={onChange} />
    <Button type="primary">Primary</Button>
    <img className='duck' src={DuckImage} />
  </div>
)

export default HomeView
