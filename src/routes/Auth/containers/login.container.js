import { connect } from 'react-redux'
import { loginAsync } from '../modules/login';

import { LoginComponent } from '../components/Login';


const mapDispatchToProps = {
    loginAsync
}


const mapStateToProps = (state) => ({
    auth: state.auth
})


export default connect(mapStateToProps, mapDispatchToProps)(LoginComponent);