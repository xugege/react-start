import {
  injectReducer
} from '../../store/reducers'


export default (store) => ({
  path: 'auth',
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const Login = require('./containers/login.container').default
      const reducer = require('./modules/login').default
      injectReducer(store, {
        key: 'auth',
        reducer
      })
      cb(null, Login)
    }, 'auth')
  }
})
