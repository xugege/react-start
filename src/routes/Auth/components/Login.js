import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Modal, Button } from 'antd';

export class LoginComponent extends Component{


    login(userName,password) { 
        const { loginAsync } = this.props;
        loginAsync(userName, password);
    }

    componentDidUpdate() {
        const { auth } = this.props;
        if (auth.success !== undefined && !auth.success) {
        }
         
    }

    render() {
        console.log('render',this)
        const { auth } = this.props;
        return (
            <div>
                <h4>Login</h4>
                <span>
                    {
                        auth.logined ? auth.user : ''
                    }
                </span>
                <button type="button" onClick={this.login.bind(this,'admin','123456')}>登录</button>
            </div>
        );

    }
}

LoginComponent.propTypes = {
    loginAsync: PropTypes.func.isRequired
}

export default LoginComponent;