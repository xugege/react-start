import {
  AppConsts
} from '../store/appConsts';
import localforage from 'localforage';
/**
 * 帮助类
 *
 * @export
 * @class Helper
 */
export class Helper {

  /**
   * 判断当前路由是否需要登录
   *
   * @static
   * @returns
   * @memberof Helper
   */
  static needLogin(route) {
    return !Helper.getToken() && route.data && route.data.auth;
  }

  static getTenantId() {
    return Cookies.getJSON(AppConsts.cookie_tenant_id);
  }

  static getToken() {
    return Cookies.getJSON(AppConsts.cookie_auth_token);
  }

  static setToken(token) {
    Cookies.set(AppConsts.cookie_auth_token, token)
  }

  /**
   * 存储登录用户信息
   *
   * @static
   * @param {any} loginResult  {studentId,studentNo,name}
   * @memberof Helper
   */
  static setLoginInfo(loginResult) {
    localforage.setItem(AppConsts.store_login_key, loginResult);
  }

  /**
   * 获取登录用户信息
   *
   * @static
   * @returns
   * @memberof Helper
   */
  static getLoginInfo() {
    return localforage.getItem(AppConsts.auth_cookie_key);
  }

}
