import 'whatwg-fetch';
import {
  AppConsts
} from './store/appConsts';

import {
  HttpProxy
} from './http/http-proxy';
import {
  Helper
} from './common/helper'

export class AppBootstraper {

  static run() {

    return new Promise((resolve, reject) => {
      fetch('/application.config.json')
        .then(function(response) {
          return response.json();
        }).then(function(json) {
          AppConsts.baseUrl = json.baseUrl;
          AppConsts.remoteApiUrl = json.remoteApiUrl;
          AppConsts.login_path = AppConsts.login_path.concat(json.login_path);
          resolve();
        }).catch((ex) => rejcet(ex));
    });
  }

  static getAll() {
    fetch(AppConsts.remoteApiUrl + 'student/getall', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json-patch+json',
        [AppConsts.cookie_tenant_id]: Helper.getTenantId(),
        [AppConsts.cookie_auth_token]: Helper.getToken()
      }
    }).then(data => {
      
      data.json().then(d => { 
        d.result && Helper.setLoginInfo(d.result);
      });
      
    }).catch(() => {});
  }

}
