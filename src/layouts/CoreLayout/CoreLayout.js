import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { IndexLink, Link } from 'react-router';
import './CoreLayout.scss';
import '../../styles/core.scss';
import { AppConsts } from '../../store/appConsts';
import {
  LoginComponent
} from '../../components/Auth/login.component';


import { Layout,Menu } from 'antd';
import { Helper } from '../../common/helper'

const { Header, Footer, Sider, Content } = Layout;

export class CoreLayout extends Component{

  constructor(props){
    super(props);
    this.state = {
      menu: AppConsts.menu
    };
  }

  componentWillMount() {
    this.requireAuth();
  }

  componentDidUpdate() {
    this.requireAuth();
  }

  requireAuth() {
    let that = this;
    let route = that.props.route;
    let r = that.props.routes[that.props.routes.length - 1];
    let canIn = !(Helper.needLogin(r));

    if (!canIn) this.props.router.push('/login?returnUrl='+this.props.location.pathname);
  }

  render(){
    let menu = this.state.menu;
    let children = this.props.children;
    return (

      <Layout className="layout" >
        <div>
          <Header className="header">
            <div className="container top-box">
              <div className="logo"></div>
            </div>
            <div className="container">
              <Menu theme={menu.theme} mode={menu.mode} style={{ lineHeight: '64px' }} defaultSelectedKeys={[menu.default]}>
                <Menu.Item key="home">
                  <Link to='/' activeClassName='route--active'>
                    Home
                  </Link>
                </Menu.Item>
                <Menu.Item key="counter">
                  <Link to='/counter' activeClassName='route--active'>
                    Counter
              </Link>
                </Menu.Item>
                <Menu.Item key="elapse">Elapse</Menu.Item>
              </Menu>
            </div>
          </Header>
          <Content className='core-layout__viewport container'>
            {children}
          </Content>
          <Footer>Footer</Footer>
        </div>

      </Layout>
    );

  }

}


CoreLayout.propTypes = {
  children: PropTypes.element.isRequired
}

export default CoreLayout;
